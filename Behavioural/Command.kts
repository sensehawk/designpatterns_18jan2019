interface Command{

    fun execute()

}

interface Light{
    
    fun on()

    fun off()

}

class RedLight: Light{

    override fun on() {
        println("turning on red-light")
    }

    override fun off(){
        println("turning off red-light")
    }

}

class LightOn(val light: Light):Command{

    override fun execute(){
        light.on()
    }

}

class LightOff(val light: Light):Command{

    override fun execute(){
        light.off()
    }

}

class RemoteControl() {

    private var command:Command? = null

    fun setCommand(command:Command){
        this.command = command
    }

    fun pressButton(){
        command?.execute()
    }

}

class Fan{

    fun on(){
        println("fan on")
    }

    fun off(){
        println("fan off")
    }

}

class FanOnCommand(val fan : Fan): Command{

    override fun execute(){
        fan.on()
    }

}

val light = RedLight()

val remote = RemoteControl()

remote.setCommand(LightOn(light))
remote.pressButton()

remote.setCommand(LightOff(light))
remote.pressButton()

val fan= Fan()
remote.setCommand(FanOnCommand(fan))
remote.pressButton()
class Rectangle{
    fun drawRectangle(x1:Int, y1:Int, width:Int, height:Int){
        println("Rectange: ($x1, $y1) ($width, $height) []")
    }
}

class Line{
    fun drawLine(x1:Int, y1:Int, x2:Int, y2:Int){
        println("Line: ($x1, $y1) ($x2, $y2) ---")
    }
}

interface Shape{
    fun draw(x: Int, y: Int, j:Int, k: Int)
}

class LineAdapter(val line:Line): Shape{
    override fun draw(x: Int, y: Int, j:Int, k: Int){
        line.drawLine(x,y, j, k)
    }
}

class RectangleAdapter(val rectangle: Rectangle):Shape{
    override fun draw(x: Int, y: Int, j: Int, k: Int){
        rectangle.drawRectangle(x, y, j, k)
    }
}

val shapes: List<Shape> = listOf(
    RectangleAdapter(Rectangle()),
    LineAdapter(Line())
    )

shapes.forEach{
    it.draw(1, 1, 2, 2)
}


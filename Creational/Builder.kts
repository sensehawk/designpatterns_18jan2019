data class Pdf(val source:String)

//abstract builder
interface ConvertionAlgoritm{

    fun convertText():ConvertionAlgoritm

    fun convertImages():ConvertionAlgoritm

    fun convertFont():ConvertionAlgoritm

    fun build():Pdf

}

//director
class PdfCreator(val convertionAlgorithm: ConvertionAlgoritm){

    fun create() : Pdf{
        return convertionAlgorithm
        .convertText()
        .convertImages()
        .convertFont()
        .build()
    }

}

//concrete builder
class WordConvertionAlgoritm(val wordFile: String) : ConvertionAlgoritm{

    override fun convertText():ConvertionAlgoritm{
        println("converting word text to pdf text")
        return this
    }

    override fun convertImages():ConvertionAlgoritm{
        println("converting word image to pdf image")
        return this
    }

    override fun convertFont():ConvertionAlgoritm{
        println("converting word font to pdf font")
        return this
    }

    override fun build():Pdf{
        return Pdf("from $wordFile")
    }

}

//html to pdf
class HtmlCovertionAlgortion(val wordFile:String): ConvertionAlgoritm{

    override fun convertText():ConvertionAlgoritm{
        println("convert htmltext to pdftext")
        return this
    }

    override fun convertImages():ConvertionAlgoritm{
        println("converting html image to pdf image")
        return this
    }

    override fun convertFont():ConvertionAlgoritm{
        println("converting html font to pdf font")
        return this
    }

    override fun build():Pdf{
        return Pdf("from $wordFile")
    }

}

val pdf1 = PdfCreator(WordConvertionAlgoritm("sample.docx")).create()
println("pdf1 = $pdf1")

println("html")
val pdf2 = PdfCreator(HtmlCovertionAlgortion("sample.html")).create()
println("pdf2 = $pdf2")

abstract class Subject{

    val observers = mutableListOf<Observer>()

    protected fun changed(){
        observers.forEach{ it.update() }
    }

}

abstract class Observer{

    fun observe(subject: Subject){
        subject.observers += this
    }

    fun retire(subject: Subject){
        subject.observers -= this
    }

    abstract fun update()

}

class Cow: Subject(){

    fun speak(text:String){
        println("moww : $text")
        changed()
    }

}

class Farmer(val name:String):Observer(){

    override fun update(){
        println("$name: What!!")
        println()
    }

}


val cow = Cow()
val farmer = Farmer("Tom")

farmer.observe(cow)

cow.speak("Hi!!!")
cow.speak("Get Lost")

farmer.retire(cow)
cow.speak("Hello!?")




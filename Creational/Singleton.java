class SimpleSingleton{

    private static SimpleSingleton INSTANCE = null;

    private String name ="NoName";

    private SimpleSingleton(String name){
        this.name = name;
    }
    
    public void printName(){
        System.out.println("name :"+name);
    }

    public static SimpleSingleton newInstance(String name){
        if(INSTANCE == null) {
            INSTANCE = new SimpleSingleton(name);
        }
        return INSTANCE;
    }

    public static void main(String args[]){
        SimpleSingleton obj1 = SimpleSingleton.newInstance("First");
        SimpleSingleton obj2 = SimpleSingleton.newInstance("Second");
        obj1.printName();
        obj2.printName();
    }

}

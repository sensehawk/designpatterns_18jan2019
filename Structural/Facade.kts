class Json(val name:String = "I am Json")
class Xml(val name:String ="I am Xml")
class MsgPk(val name:String = "I am MsgPack")


class JsonToXmlConvertor(){
    
    fun convertToXml(json: Json) = Xml()

}

class MsgPkToJsonConvertor(){

    fun converToJson(msgPk: MsgPack) =Json()

}

class MsgPkToXmlConvertor(){

    init{
        val msgPkToJsonConvertor = MsgPkToJsonConvertor()
        val jsonToXmlConvertor = JsonToXmlConvertor()
    }

    fun convertToXml(msgPk: MsgPac): Xml{
        val json = msgPkToJsonConvertor.converToJson(msgPk)
        val xml = jsonToXmlConvertor.convertToXml(json)
        return xml
    }

}
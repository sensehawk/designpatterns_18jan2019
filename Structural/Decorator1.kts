interface Car{
    fun assemble()
}

class SimpleCar(): Car{

    override fun assemble(){
        println("making simple car")
    }

}

open class CarDecorator(val car:Car): Car{

    override fun assemble(){
        car.assemble()
    }

}

class SportsCar(car:Car): CarDecorator(car){

    override fun assemble(){
        super.assemble()
        println("adding sports car features")
    }
    
}

class LuxuryCar(car:Car): CarDecorator(car){

    override fun assemble(){
        super.assemble()
        println("adding features of LuxuryCar")
    }

}

val car:Car = SportsCar(LuxuryCar(SimpleCar()))
car.assemble()



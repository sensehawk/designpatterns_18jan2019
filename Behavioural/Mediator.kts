interface ChatRoomMediator{

    fun sendMessage(sender:User, message:String)

    fun addUser(user: User)

    fun removeUser(user: User)

}

class User(val mediator:ChatRoomMediator,val name:String){

    init{
        mediator.addUser(this)
    }

    fun send(message:String){
        mediator.sendMessage(this, message)
    }

    fun receive(message:String){
        println("$name: ReceivedMessage [$message]")
    }

}

class ChatRoomMediatorImpl : ChatRoomMediator{

    val users:MutableList<User> = mutableListOf()

    override fun sendMessage(sender:User,message:String){
        users.filter{ it != sender }.forEach{ it.receive(message)}
    }

    override fun addUser(user: User){
        users += user
    }

    override fun removeUser(user: User){
        users -= user
    }
}

val teamArrow:ChatRoomMediator = ChatRoomMediatorImpl()

val oliver = User(teamArrow, "Oliver")
val john = User(teamArrow, "John")
val felicity = User(teamArrow, "Felicity")

oliver.send("Suit Up!!!")
println()
john.send("Shut Up!!")



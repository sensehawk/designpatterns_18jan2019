interface Internet{

    fun connectTo(url:String) 

}

class NormalInternet : Internet{

    override fun connectTo(url:String) {
        println("Normal Internet:")
        println("Connecting to $url")
    }

}

class ProxyInternet: Internet{

    private val normalInternet = NormalInternet()
    private val urlList = ArrayList<String>()

    init{
        urlList += "www.google.com"
    }

    override fun connectTo(url:String) {
        println("Proxy Internet:")
        if(url in urlList){
            println("Error: Restricted Site")
            return
        }
        normalInternet.connectTo(url)

    }

}

val internet1 = NormalInternet()
internet1.connectTo("www.google.com")
println()
val internet2 = ProxyInternet()
internet2.connectTo("www.google.com")
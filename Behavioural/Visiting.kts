abstract class Product{

    //manufacture cost
    abstract fun mnfc():Double

    //tax %
    abstract fun taxPtg(): Double

    fun totalCost(): Double{
        println("manufacture : ${mnfc()}")
        val tax = mnfc() * (taxPtg()/100.0)
        println("tax : $tax")
        return mnfc() + tax
    }

}

//
class Book: Product(){

    override fun mnfc() = 40.0

    override fun taxPtg() = 20.0

}

class Milk: Product(){

    override fun mnfc() =  100.0

    override fun taxPtg() = 5.0

}

class Sugar: Product(){

    override fun mnfc() =  300.0

    override fun taxPtg() = 2.0

}

val listOfProducts = listOf(Book(), Milk(), Sugar())
var cartCost = 0.0
for (product in listOfProducts){
    println("${product::class.qualifiedName}")
    cartCost += product.totalCost()
}
println("total cost: $cartCost ")
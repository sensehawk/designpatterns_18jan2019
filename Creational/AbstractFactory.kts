//abstracts
interface Theme{
    fun apply()
}

//header
//body
//footer

//concretes
//dark
class DarkTheme:Theme{
    override fun apply(){
        println("DarkTheme")
    }
}
//light
class LightTheme:Theme{
    override fun apply(){
        println("LightTheme")
    }
}

//factory
interface ThemeFactory{
    
    fun create(theme:String):Theme
}

class SimpleThemeFactory: ThemeFactory{
    
    override fun create(theme:String): Theme{
        return when(theme){
            "light" -> LightTheme()
            "dark" -> DarkTheme()
            else -> throw Exception("wtf")
        }
    }

}

//Theme store
class ThemeStore(val factory: ThemeFactory){

    public fun applyTheme(theme:String){
        val theme1:Theme = factory.create(theme)
        theme1.apply()
    }

}

//main
ThemeStore(CustomThemeFactory()).applyTheme("grey")
ThemeStore(SimpleThemeFactory()).applyTheme("dark")

//curstom theme
//grey
class GreyTheme:Theme{
    override fun apply(){
        println("GreyTheme")
    }
}

class CustomThemeFactory: ThemeFactory{
    
    override fun create(theme:String): Theme{
        return when(theme){
            "grey" -> GreyTheme()
            else -> throw Exception("wtf")
        }
    }

}



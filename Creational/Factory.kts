data class Image(val imageName: String)

interface ImageReader {

    fun readImage(): String

}

class GifImageReader(val image: Image) : ImageReader {
    override fun readImage(): String = "from GifReader: $image"
}

class JpgImageReader(val image: Image) : ImageReader {
    override fun readImage(): String = "from JpgReader: $image"

}

class ImageReaderFactory {
    companion object {
        fun create(image: Image): ImageReader {
            val extension = image.imageName.split(".")[1]
            return when (extension.toLowerCase()) {
                "gif" -> GifImageReader(image)
                "jpg" -> JpgImageReader(image)
                "png" -> PngImageReader(image)
                else -> throw Exception("wtf")
            }

        }
    }
}

val image1 = Image("sample.jpg")
val image2 = Image("sample.gif")

val imageReader1 = ImageReaderFactory.create(image1)
val imageReader2 = ImageReaderFactory.create(image2)
println(imageReader1.readImage())
println(imageReader2.readImage())

//

class PngImageReader(val image: Image) : ImageReader {
    override fun readImage(): String = "from PngReader: $image"

}

val image3 = Image("sample.png")

val imageReader3 = ImageReaderFactory.create(image3)

println(imageReader3.readImage())

//